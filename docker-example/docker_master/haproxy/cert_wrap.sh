#!/bin/bash
set -e
for i in $(ps -ef | grep "\/usr\/sbin\/haproxy" | awk '{ print $2 }');
do
        kill -SIGTERM $i
done
/cert.sh "$@"
sleep 1s
/usr/sbin/haproxy -f /etc/haproxy/haproxy.conf
