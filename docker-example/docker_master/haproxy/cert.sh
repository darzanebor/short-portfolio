#!/bin/bash
email="info@muffs.ru"
iptables_check=0
main_dir=/certs
if [ $# -eq 0 ]; then
    clear
    echo "No arguments provided! Usage $0 name.com a.name.com b.name.com"        
    exit 1
fi

if [[ $(which iptables) != '' ]];
then
	iptables_check=1
fi

if [ "$iptables_check" == 1 ];
then
	port_state=$(iptables -nL --line-numbers | grep "^\d*.*ACCEPT.*tcp.*dpt:80.*ctstate.*NEW\,ESTABLISHED$" | awk '{ print $2 }')
	if [ ${port_state:-0} == "ACCEPT" ];
	then
		echo "port check - OK;"
	else
		echo "opening port 80;"
		iptables -I INPUT 1 -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
	fi
fi

domains=""
for i in "${@:1}";
do
 domains="$domains -d $i"
done

echo "Generating certificates for: $(echo $domains | sed 's/\-d//g')"
#rm -rf /certs/config/archive/*
#rm -rf  /certs/config/live/*

/opt/certbot-auto certonly --standalone --config-dir $main_dir/config --logs-dir $main_dir/logs --work-dir $main_dir/workdir  $domains --preferred-challenges http --agree-tos -n -m $email --keep-until-expiring --debug --no-self-upgrade

#--expand

cert_path=/certs/config/archive
folders=$(ls -1 /certs/config/archive | grep -v README | grep -E .*-00.* | sed '/^$/d')

for folder in $folders;
do
  if [[ $folder =~ ^.*\-.*$ ]]; then
    real_name=$(echo $folder | cut -d '-' -f1)
    mv -f $cert_path/$folder $cert_path/$real_name
  fi
done


dir=$main_dir/done/$1
all_pem=$main_dir/done/all
if [ ! -d $dir ];
then
  mkdir -p $dir
fi

if [ ! -d $all_pem ];
then
  mkdir -p $all_pem
else
  cp -R $all_pem $all_pem.old
fi

if ls -1 $main_dir/config/archive/$1 | grep -E .*[0-9].*; then
  for name in $(ls -1 $main_dir/config/archive/$1);
    do
        mv $main_dir/config/archive/$1/$name $main_dir/config/archive/$1/$(echo $name | sed 's/[0-9]*//g')
    done
fi

#Place certs
cat $main_dir/config/archive/$1/chain.pem > $dir/chain.pem
cat $main_dir/config/archive/$1/cert.pem $main_dir/config/archive/$1/privkey.pem > $dir/cert-key.pem
cat $dir/cert-key.pem $dir/chain.pem > $all_pem/$1.pem

if [ "$iptables_check" == 1 ];
then
	port_state=$(iptables -nL --line-numbers | grep "^\d*.*ACCEPT.*tcp.*dpt:80.*ctstate.*NEW\,ESTABLISHED$" | awk '{ print $2 }')
	if [ ${port_state:-0} == "ACCEPT" ];
	then
	  echo "closing port 80;"
	  num=$(iptables -nL --line-numbers | grep "^\d*.*ACCEPT.*tcp.*dpt:80.*ctstate.*NEW\,ESTABLISHED$" | awk '{ print $1 }')
	  iptables -D INPUT $num
	fi
fi
echo "done with $(echo $domains | sed 's/\-d//g')"
