#!/bin/bash
set -e
if [ -f /cert.sh ];
then
    chmod +x /cert.sh
fi

if [ -f /var/run/rsyslogd.pid ];
then
    rm -f /var/run/rsyslogd.pid
fi

if [ -f /var/run/suricata.pid ];
then
   rm -f /var/run/suricata.pid
fi

#chown -R root:root /etc/metricbeat

if [ $# -eq 0 ];
then
    /usr/sbin/rsyslogd -n &
    /usr/bin/suricata -D -c /etc/suricata/suricata.yaml -i eth1 &
    /usr/bin/metricbeat run &
    /usr/sbin/haproxy -f /etc/haproxy/haproxy.conf
    sleep 1000d
else
    /usr/sbin/rsyslogd -n &
    /usr/bin/suricata -D -c /etc/suricata/suricata.yaml -i eth1 &
    /usr/bin/metricbeat run &
    /usr/sbin/haproxy -f $1
    sleep 1000d
fi
