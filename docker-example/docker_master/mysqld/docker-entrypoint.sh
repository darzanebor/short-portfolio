#!/bin/bash
set -e

mysql_config_file=/etc/mysql/my.cnf
galera_config_file=/etc/mysql/conf.d/galera.cnf

# set timezone if it was specified
if [ -n "$TIMEZONE" ]; then
	echo ${TIMEZONE} > /etc/timezone && \
	dpkg-reconfigure -f noninteractive tzdata
fi

# apply environment configuration
sed -i 's/.*bind-address\s*=\s*127.0.0.1/bind-address=0.0.0.0/g' $mysql_config_file

# check if galera.cnf exists if no then copy and create
if [ ! -f $galera_config_file ];
then
    cp $galera_config_file".bak" $galera_config_file
    sed -i "s/{{ this_node_ip }}/$(hostname -i)/g" $galera_config_file
    sed -i "s/{{ this_node_name }}/$(hostname)/g"  $galera_config_file
fi




# logs for build and testing purposes
#sed -i 's/#general_log_file/general_log_file/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/#general_log/general_log/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/#slow_query_log_file/slow_query_log_file/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/#long_query_time/long_query_time/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/#log-queries-not-using-indexes/log-queries-not-using-indexes/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#echo "log_slow_queries" >> /etc/mysql/mariadb.conf.d/50-server.cnf

if [ ! -d /var/run/mysqld ];
then
    mkdir /var/run/mysqld
    chown -R mysql:mysql /var/run/mysqld
fi

if [ -f /var/run/mysqld/mysqld.sock ]
then 
    rm /var/run/mysqld/mysqld.sock
fi

if [ ${1:-0} = "galera_create_cluster" ]
then    
    /usr/sbin/mysqld --wsrep-new-cluster
    #echo "SHOW STATUS LIKE 'wsrep_cluster_%'" | mysql -u root
    #echo "CREATE USER 'haproxy_user'@'$(arp -a sql-haproxy-01 | awk '{ print $2 }' | sed 's/(//g' | sed 's/)//g')';" | mysql -u root
    #echo "GRANT ALL PRIVILEGES ON *.* TO 'haproxy_user'@'$(arp -a sql-haproxy-01 | awk '{ print $2 }' | sed 's/(//g' | sed 's/)//g')' WITH GRANT OPTION;" | mysql -u root
    
    #SET GLOBAL wsrep_provider_options='pc.bootstrap=1';
    #sed -i 's/safe_to_bootstrap: 0/safe_to_bootstrap: 1/g' /var/lib/mysql/grastate.dat
else
    /usr/sbin/mysqld
fi
#sleep 1000d