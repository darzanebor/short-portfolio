#!/bin/bash
chown -R named:named /var/log/named
chown -R named:named /etc/bind
chmod -R 550 /etc/bind
chmod -R 750 /etc/bind/zones
chmod -R 750 /var/log/named
if [ -d /run/named ];
then
  mkdir -p /run/named
  chown -R  named:named /run/named
fi
named -c /etc/bind/named.conf -4 -f -u named
