#!/bin/bash
chown -R named:named /var/log/named
chown -R named:named /etc/bind
named -c /etc/bind/named.conf -g -u named
