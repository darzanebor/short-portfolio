#!/bin/bash
chown -R www-data:www-data /var/www
chown -R www-data:www-data /var/log/lighttpd
chmod -R 550 /var/www
chmod -R 750 /var/log/lighttpd
if [ ! -d "/var/run/lighttpd" ]; then
	mkdir /var/run/lighttpd
	chown -R www-data:www-data /var/run/lighttpd
        chmod -R 750 /var/run/lighttpd
fi
lighttpd -D -f /etc/lighttpd/lighttpd.conf
sleep 1000d
